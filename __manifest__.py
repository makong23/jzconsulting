# -*- coding: utf-8 -*-

{
    'name': 'JZConsulting',
    'version': '11.0.1.0.1',
    'category': 'Contact',
    'author': 'Thomas',
    'website': '',
    'license': 'AGPL-3',
    'summary': """Améliorer et complète les informations de la fiche client""",
    'description': '',
    'depends': [
        'base'],
    'data': [
        'security/consulting_security.xml',
        'security/ir.model.access.csv',
        'views/res_users_views.xml',
        'report/fiche_client.xml',
    ],
    
    'application': False,
    'installable': True,
    'auto_install': False,
}
