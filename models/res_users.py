# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import date, datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from dateutil.relativedelta import relativedelta


class ResPart(models.Model):
    _inherit = 'res.partner'

    nomfam = fields.Char(string='Autres noms de famille')
    prenomfam = fields.Char(string='Autres prenoms')
    q_marie = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    sexe = fields.Selection([('Masculin', 'Masculin'), ('Féminin', 'Féminin')], default='Masculin')
    age = fields.Integer(string='Age', compute='compute_age', readonly=True)
    patient_dob = fields.Date(string="Date de naissance", required=True)
    date = fields.Datetime(string='Date Requested', default=lambda s: fields.Datetime.now(), invisible=True)
    nationality_id = fields.Many2one('res.country', string='Pays', default=47)
    region = fields.Many2one('contact.region', string='Région')
    ville = fields.Many2one('contact.ville', string='Ville')
    #ville = fields.Many2one('contact.ville', string='Ville', domain="[('region', '=?', region)]")
    matrimonial = fields.Selection([('Célibataire', 'Célibataire'), ('Marié', 'Marié'), ('Conjoint de fait', 'Conjoint de fait'), ('Séparé', 'Séparé')
                             ,('Divorcé', 'Divorcé'), ('Veuf', 'Veuf'), ('Mariage annulé', 'Mariage annulé')], default='Célibataire')
    datemariage = fields.Date(string='Date de mariage')
    nbre_marie = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    phone2 = fields.Char(string='Téléphone au travail')
    nationalite = fields.Many2one('contact.nation', string='Citoyenneté / Nationalité')

    ncarte = fields.Char(string='Numéro de la carte')
    payscarte = fields.Char(string='Pays de délivrance')
    datecartedel = fields.Date(string='Date de délivrance')
    datecarteexp = fields.Date(string='Date d\'expiration')

    npp = fields.Char(string='Numéro du passeport')
    payspp = fields.Char(string='Pays de délivrance')
    dateppdel = fields.Date(string='Date de délivrance')
    dateppeexp = fields.Date(string='Date d\'expiration')

    datecanada = fields.Date(string='Date de votre dernière entrée au Canada')
    lieucanada = fields.Char(string='Lieu de votre dernière entrée au Canada')
    langue = fields.Selection([('Français', 'Français'), ('Anglais', 'Anglais'), ('Français et Anglais', 'Français et Anglais')], default='Français')
    situation = fields.Selection(
        [('Aux études', 'Aux études'), ('En emploi', 'En emploi'), ('Sans emploi', 'Sans emploi'), ('Autre', 'Autre')], default='Aux études')
    autresituation = fields.Char(string='Autre situation')
    provincecanada = fields.Char(string='Province canada')
    villecanada = fields.Char(string='Ville canada')
    profession = fields.Char(string='Profession envisagé au canada')

    adresseline = fields.One2many('adresse.lines', 'adresse_id', ondelete='cascade')

    scolprim = fields.Integer(string='Élémentaire /École primaire')
    scolsecond = fields.Integer(string='Secondaire')
    scoluniver = fields.Integer(string='Université')
    scolform = fields.Integer(string='École de formation professionnelle ou autre école postsecondaire')
    scolariteline = fields.One2many('scolarite.lines', 'scolarite_id', ondelete='cascade')
    emploisline = fields.One2many('emplois.lines', 'emplois_id', ondelete='cascade')
    familleline = fields.One2many('famille.lines', 'famille_id', ondelete='cascade')
    familleparline = fields.One2many('famillepar.lines', 'famillepar_id', ondelete='cascade')
    famillerline = fields.One2many('familler.lines', 'familler_id', ondelete='cascade')
    familleadline = fields.One2many('famillead.lines', 'famillead_id', ondelete='cascade')
    famillefreline = fields.One2many('famillefre.lines', 'famillefre_id', ondelete='cascade')
    famillecanline = fields.One2many('famillecan.lines', 'famillecan_id', ondelete='cascade')
    sejourline = fields.One2many('sejour.lines', 'sejour_id', ondelete='cascade')
    diplomeline = fields.One2many('diplome.lines', 'diplome_id', ondelete='cascade')

    q_temporaire = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    province1 = fields.Many2one('contact.province', string='Province')
    date_temp = fields.Date('Date')
    q_decision = fields.Selection(
        [('Acceptée', 'Acceptée'), ('Refusée', 'Refusée'), ('En attente décision', 'En attente décision')], default='En attente décision')

    q_permanentequebec = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    q_decision2 = fields.Selection(
        [('Acceptée', 'Acceptée'), ('Refusée', 'Refusée'), ('En attente décision', 'En attente décision')], default='En attente décision')
    q_programme = fields.Selection(
        [('Programme régulier', 'Programme régulier'), ('PEQ', 'PEQ'), ('Autre', 'Autre')])
    autre = fields.Char(string='Autre')

    q_permanente = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    province3 = fields.Many2one('contact.province', string='Province')
    date_perm = fields.Date('Date')
    q_decision3 = fields.Selection(
        [('Acceptée', 'Acceptée'), ('Refusée', 'Refusée'), ('En attente décision', 'En attente décision')], default='En attente décision')

    q_asile = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    province4 = fields.Many2one('contact.province', string='Province')
    date_asile = fields.Date('Date')
    q_decision4 = fields.Selection(
        [('Acceptée', 'Acceptée'), ('Refusée', 'Refusée'), ('En attente décision', 'En attente décision')], default='En attente décision')

    emploican = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    nomentrep = fields.Char(string='Nom de l\'entreprise')
    adreentrep = fields.Char(string='Adresse de l\'entreprise')
    titposte = fields.Char(string='Titre du poste')
    salposte = fields.Char(string='Salaire du poste')

    postepublic = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    precision37 = fields.Char(string='Précision')
    membreorg = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    precision38 = fields.Char(string='Précision')
    membreforce = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    precision39 = fields.Char(string='Précision')
    couplable40 = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    precision40 = fields.Char(string='Précision')
    couplable41 = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    precision41 = fields.Char(string='Précision')
    couplable42 = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    precision42 = fields.Char(string='Précision')
    asile43 = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    precision43 = fields.Char(string='Précision')
    refugie44 = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    precision44 = fields.Char(string='Précision')
    refus45 = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    precision45 = fields.Char(string='Précision')
    genocide46 = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    precision46 = fields.Char(string='Précision')
    lutte47 = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    precision47 = fields.Char(string='Précision')
    lutte48 = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    precision48 = fields.Char(string='Précision')
    criminelle49 = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    precision49 = fields.Char(string='Précision')
    detention50 = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    precision50 = fields.Char(string='Précision')
    maladie51 = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
    precision51 = fields.Char(string='Précision')

    organismes = fields.Many2one('contact.organisme', string='Organismes designés et choisis')
    date_equidipl = fields.Date('Date demande d\'équivalences de diplomes', dfault=fields.date)
    date_equifin = fields.Date('Date d\'estimation de la fin d\'equivalence', readonly=True)
    date_obtention = fields.Date('Date d\'obtention du rapport d\'équivalence de diplomes')
    progressionbar = fields.Integer('Progression de la demande', default=0, compute='compute_dateau')
    maximum_rate = fields.Integer(string='max', default=100)
    value1 = fields.Integer(string='value1', default=0)
    value2 = fields.Integer(string='value2', default=0)
    datecurrent_dt = fields.Date(string='Date du jour', dfault=fields.date, compute='compute_dateau')

    examenlang = fields.Selection([('TEF', 'TEF Canada'), ('TCF', 'TCF Canada'), ('IELTS', 'IELTS GENERAL TRAINING')], default='TEF')
    centref = fields.Selection([('IFD', 'Institut Français de Douala'), ('TCY', 'Institut Français de Yaoundé'),
                                ('AFD', 'Alliance Français de Dschang'), ('UB', 'Université de Buea'), ('AFG', 'Alliance Français de Garoua'), ('Autres', 'Autres')], default='IFD')
    centfautre = fields.Char(string='Précisez')
    centrea = fields.Selection([('BCD', 'BRITISH COUNCIL DOUALA'), ('BCY', 'BRITISH COUNCIL YAOUNDE'), ('Autres', 'Autres')], default='BCD')
    centanautre = fields.Char(string='Précisez')
    date_ins = fields.Date('Date d\'inscription')
    date_obtres = fields.Date('Date d\'obtention de resultat')
    numero_ins = fields.Char('Numéro d\'inscription du client')
    numero_form = fields.Char('Numéro de formulaire')
    tefeo = fields.Selection([('393-450', '393-450'),('371-392', '371-392'),('349-370', '349-370'),('310-348', '310-348'),
                              ('271-309', '271-309'), ('226-270', '226-270'), ('181-225', '181-225'),('0-180', '0-180')])
    tefco = fields.Selection([('316-360', '316-360'),('298-315', '298-315'),('280-297', '280-297'),('249-279', '249-279'),
                             ('217-248', '217-248'),('181-216', '181-216'),('145-180', '145-180'),('0-144', '0-144')])
    tefce = fields.Selection([('263-300', '263-300'),('248-262', '248-262'),('233-247', '233-247'),('207-232', '207-232'),
                             ('181-206', '181-206'),('151-180', '151-180'),('121-150', '121-150'),('0-120', '0-120'),])
    tefee = fields.Selection([('393-450', '393-450'),('371-392', '371-392'),('349-370', '349-370'),('310-348', '310-348'),
                              ('271-309', '271-309'), ('226-270', '226-270'), ('181-225', '181-225'),('0-180', '0-180')])
    tcfeo = fields.Selection([('16-20', '16-20'),('14-15', '14-15'),('12-13', '12-13'),('10-11', '10-11'),('6', '6'),('4-5', '4-5'),('0-3', '0-3')])
    tcfco = fields.Selection([('549-699', '549-699'),('523-548', '523-548'),('503-522', '503-522'),('458-502', '458-502'),
                              ('398-457', '398-457'),('369-397', '369-397'), ('331-368', '331-368'), ('0-330', '0-330')])
    tcfce = fields.Selection([('549-699', '549-699'),('524-548', '524-548'),('499-523', '499-523'),('453-498', '453-498'),
                              ('406-452', '406-452'),('375-405', '375-405'),('342-374', '342-374'),('0-341', '0-341')])
    tcfee = fields.Selection([ ('16-20', '16-20'),('14-15', '14-15'),('12-13', '12-13'),('10-11', '10-11'),('7-9', '7-9'),
                               ('6', '6'),('4-5', '4-5'),('0-3', '0-3')])
    ieltsspea = fields.Selection([ ('7.5-9.0', '7.5-9.0'), ('7.0', '7.0'),('6.5', '6.5'),('6.0', '6.0'), ('5.5', '5.5'),
                                 ('5.0', '5.0'), ('4.0-4.5', '4.0-4.5'), ('0-3.5', '0-3.5')])
    ieltslist = fields.Selection([('8.5-9.0', '8.5-9.0'),('8.0', '8.0'),('7.5', '7.5'),('6.0-7.0', '6.0-7.0'),('5.5', '5.5'),
                                  ('5.0', '5.0'),('4.5', '4.5'),('0-4.0', '0-4.0')])
    ieltsread = fields.Selection([('8.0-9.0', '8.0-9.0'),('7.0-7.5', '7.0-7.5'),('6.0', '6.0'),('5.0-5.5', '5.0-5.5'),
                                  ('4.0-4.5', '4.0-4.5'),('3.5', '3.5'),('0-3.0', '0-3.0')])
    ieltswrit = fields.Selection([('7.5-9.0', '7.5-9.0'),('7.0', '7.0') , ('6.5', '6.5'), ('6.0', '6.0'),
                                 ('5.5', '5.5'),('5.0', '5.0'),('4.0-4.5', '4.0-4.5'),('0-3.5', '0-3.5')])


    @api.multi
    @api.depends('datecurrent_dt','date_equidipl')
    def compute_dateau(self):
        for rec in self:
            rec.datecurrent_dt = date.today()
            if rec.date_equidipl:
                if rec.datecurrent_dt:
                    start = datetime.strptime(rec.date_equidipl, '%Y-%m-%d')
                    start1 = datetime.strptime(rec.datecurrent_dt, '%Y-%m-%d')
                    date_calc = start + relativedelta(months=5)
                    rec.date_equifin = date_calc
                    rec.value1 = (date_calc - start).days
                    rec.value2 = (start1 - start).days
                    progression = rec.value2 * 100 / rec.value1
                    if progression > 100:
                      rec.date_equifin = date_calc
                      rec.progressionbar = 100
                    elif progression < 0:
                      rec.date_equifin = date_calc
                      rec.progressionbar = 0
                    elif progression < 100:
                      rec.date_equifin = date_calc
                      rec.progressionbar = rec.value2 * 100 / rec.value1


    class ContactRegion(models.Model):
        _name = 'contact.region'
        _rec_name = 'nom'

        nom = fields.Char('Nom')
        city = fields.Many2one('contact.ville', 'ville')
        _sql_constraints = [
            ('nom_uniq', 'unique (nom)', 'Ce région existe déja!')
        ]

        @api.depends('nom')
        def UppercaseNameNeighborhood(self):
            for record in self:
                if record.nom != 0:
                    record.nom = record.nom.upper().encode('utf-8')
                    record.name = record.nom

    class ContactVille(models.Model):
        _name = 'contact.ville'
        _rec_name = 'nom'

        nom = fields.Char('Nom de la ville')
        _sql_constraints = [
            ('nom_uniq', 'unique (nom)', 'Cette ville  existe déja!')
        ]

    class ContactNationalite(models.Model):
        _name = 'contact.nation'
        _rec_name = 'nom'

        nom = fields.Char('Nom')
        _sql_constraints = [
            ('nom_uniq', 'unique (nom)', 'Cette nationalité  existe déja!')
        ]

    class ContactLien(models.Model):
        _name = 'contact.lien'
        _rec_name = 'nom'

        nom = fields.Char('Nom')
        _sql_constraints = [
            ('nom_uniq', 'unique (nom)', 'Ce lien existe déja!')
        ]

    class ContactProvince(models.Model):
        _name = 'contact.province'
        _rec_name = 'nom'

        nom = fields.Char('Nom')
        _sql_constraints = [
            ('nom_uniq', 'unique (nom)', 'Cette province existe déja!')
        ]

    class ContactSejour(models.Model):
        _name = 'contact.sejour'
        _rec_name = 'nom'

        nom = fields.Char('Nom')
        _sql_constraints = [
            ('nom_uniq', 'unique (nom)', 'Cette province existe déja!')
        ]

    class ContactOrganisme(models.Model):
        _name = 'contact.organisme'
        _rec_name = 'nom'

        nom = fields.Char('Nom')
        _sql_constraints = [
            ('nom_uniq', 'unique (nom)', 'Organisme existe déja!')
        ]


    # fonction qui calcule l'age 2
    @api.multi
    @api.depends('datecurrent_dt','patient_dob')
    def compute_age(self):
        '''Method to calculate student age'''
        for rec in self:
            if rec.patient_dob:
                if rec.datecurrent_dt:
                    start = datetime.strptime(rec.patient_dob,
                                              '%Y-%m-%d')
                    start1 = datetime.strptime(rec.datecurrent_dt, '%Y-%m-%d')
                    age_calc = ((start1 - start).days / 365)
                    # Age should be greater than 0
                    if age_calc > 0.0:
                        rec.age = age_calc

    # Fonction qui fait en sorte que l'age ne doit pas etre supérieur à 2
    @api.constrains('patient_dob')
    def check_age(self):
        '''Method to check age should be greater than 2'''
        current_dt = datetime.today()
        if self.patient_dob:
            start = datetime.strptime(self.patient_dob, DEFAULT_SERVER_DATE_FORMAT)
            age_calc = ((current_dt - start).days / 365)
            # Check if age less than 2 years
            if age_calc < 0:
                raise ValidationError(
                    _('''Date de naissance impossible à valider'''))

    class AdresseLines(models.Model):
        _name = 'adresse.lines'

        datedebut = fields.Date('Du')
        datefin = fields.Date('Au')
        nationality_id = fields.Many2one('res.country', string='Pays', default=47)
        region = fields.Many2one('contact.region', string='Region')
        ville = fields.Many2one('contact.ville', string='Ville')
        cdpostal = fields.Char(string='code postal')
        rue = fields.Char(string='Rue')
        adresse_id = fields.Many2one('res.partner', string="Adresses")

        @api.depends('datedebut','datefin')
        def compute_amount(self):
            for record in self:
                record.datedebut = record.datedebut.strptime('%B %Y')
                record.datefin = record.datefin.strptime('%B %Y')

    class ScolariteLines(models.Model):
        _name = 'scolarite.lines'

        datedebut = fields.Date('Du')
        datefin = fields.Date('Au')
        etablis = fields.Char(string='établissement')
        diplome = fields.Char(string='diplôme')
        villepays = fields.Char(string='Ville et pays')
        special = fields.Char(string='Spécialisation')
        obtention = fields.Date('Année d\'obtention')
        langue = fields.Selection([('Français', 'Français'), ('Anglais', 'Anglais'), ('Français et Anglais', 'Français et Anglais')], default='Français')
        temps = fields.Selection([('temps plein', 'temps plein'), ('temps partiel', 'temps partiel')], default='temps plein')
        scolarite_id = fields.Many2one('res.partner', string="Scolarité")

        @api.depends('datedebut','datefin','obtention')
        def compute_amount(self):
            for record in self:
                record.datedebut = record.datedebut.strptime('%B %Y')
                record.datefin = record.datefin.strptime('%B %Y')
                record.obtention = record.datefin.strptime('%Y')

    class EmploisLines(models.Model):
        _name = 'emplois.lines'

        datedebut = fields.Date('Du')
        datefin = fields.Date('Au')
        entreprise = fields.Char(string='Entreprise')
        titre = fields.Char(string='Titre')
        hsemaine = fields.Char(string='heure/Semaine')
        tache = fields.Text(string='Tache effectué')
        villepays = fields.Char(string='Ville et pays')
        langue = fields.Selection([('Français', 'Français'), ('Anglais', 'Anglais'), ('Français et Anglais', 'Français et Anglais')], default='Français')
        emplois_id = fields.Many2one('res.partner', string="Emplois")

    class FamilleLines(models.Model):
        _name = 'famille.lines'

        nom = fields.Char(string='Nom à la naissance')
        prenom = fields.Char(string='Prénom')
        lien = fields.Many2one('contact.lien', string='Lien de parenté')
        date_naiss = fields.Date('Date naissance')
        ville = fields.Many2one('contact.ville', string='Ville')
        pays = fields.Many2one('res.country', string='Pays')
        civil = fields.Selection(
            [('Célibataire', 'Célibataire'), ('Marié', 'Marié'), ('Conjoint de fait', 'Conjoint de fait'),
             ('Séparé', 'Séparé')
                , ('Divorcé', 'Divorcé'), ('Veuf', 'Veuf'), ('Mariage annulé', 'Mariage annulé')])

        accompagner = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
        habiter = fields.Selection([('O', 'oui'), ('N', 'non')], default='N')
        famille_id = fields.Many2one('res.partner', string="Famille")

    class FamillerLines(models.Model):
        _name = 'familler.lines'

        nom = fields.Char(string='Nom à la naissance')
        prenom = fields.Char(string='Prénom')
        raison = fields.Text(string='Raison pourquoi ce membre de votre famille ne vous accompagne pas')
        familler_id = fields.Many2one('res.partner', string="Famille")

    class FamilleadLines(models.Model):
        _name = 'famillead.lines'

        nom = fields.Char(string='Nom à la naissance')
        prenom = fields.Char(string='Prénom')
        dateadop = fields.Date(string='Date de l\'adoption')
        famillead_id = fields.Many2one('res.partner', string="Famille")

    class FamilleparLines(models.Model):
        _name = 'famillepar.lines'

        nom = fields.Char(string='Nom à la naissance')
        prenom = fields.Char(string='Prénom')
        parent = fields.Selection([('Père', 'Père'), ('Mère', 'Mère'), ('Autre', 'Autre')])
        date_naiss = fields.Date('Date naissance')
        ville = fields.Many2one('contact.ville', string='Ville')
        pays = fields.Many2one('res.country', string='Pays')
        civil = fields.Selection(
            [('Célibataire', 'Célibataire'), ('Marié', 'Marié'), ('Conjoint de fait', 'Conjoint de fait'),
             ('Séparé', 'Séparé')
                , ('Divorcé', 'Divorcé'), ('Veuf', 'Veuf'), ('Mariage annulé', 'Mariage annulé')])
        adresses = fields.Char(string='Adresse courante')
        date_dec = fields.Date('Date du décès')
        famillepar_id = fields.Many2one('res.partner', string="Famille")

    class FamillefreLines(models.Model):
        _name = 'famillefre.lines'

        nom = fields.Char(string='Nom à la naissance')
        prenom = fields.Char(string='Prénom')
        fresoeur = fields.Selection([('Frère', 'Frère'), ('Soeur', 'Soeur')])
        date_naiss = fields.Date('Date naissance')
        ville = fields.Many2one('contact.ville', string='Ville')
        pays = fields.Many2one('res.country', string='Pays')
        civil = fields.Selection(
            [('Célibataire', 'Célibataire'), ('Marié', 'Marié'), ('Conjoint de fait', 'Conjoint de fait'),
             ('Séparé', 'Séparé')
                , ('Divorcé', 'Divorcé'), ('Veuf', 'Veuf'), ('Mariage annulé', 'Mariage annulé')])
        adresses = fields.Char(string='Adresse courante')
        famillefre_id = fields.Many2one('res.partner', string="Famille")

    class FamillecanLines(models.Model):
        _name = 'famillecan.lines'

        nom = fields.Char(string='Nom à la naissance')
        prenom = fields.Char(string='Prénom')
        lien = fields.Many2one('contact.lien', string='Lien de parenté')
        adresses = fields.Char(string='Adresse complète')
        phone = fields.Char(string='Numéro de téléphone')
        nbreannee = fields.Integer(string='Nombre d\'années au Canada')
        famillecan_id = fields.Many2one('res.partner', string="Famille")

    class SejourLines(models.Model):
        _name = 'sejour.lines'

        description = fields.Many2one('contact.sejour', string='Description')
        date_arr = fields.Date('Date d\'arrivée')
        date_dep = fields.Date('Date de départ')
        province = fields.Many2one('contact.province', string='Province')
        sejour_id = fields.Many2one('res.partner', string="Séjour au canada")

    class diplomeLines(models.Model):
        _name = 'diplome.lines'

        description = fields.Char(string='liste des dipomes et relévé de notes')
        diplome_id = fields.Many2one('res.partner', string="Diplome et releve")